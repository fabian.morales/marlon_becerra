(function(window, $){
    function scrollTo(target) {
        var wheight = $(window).height();
        
        var ooo = $(target).offset().top;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    $(document).ready(function() {
        $(window).resize(function() {                
            if ($("#lnkPoliticas").is(":visible")){
                $("#politicas").hide();
            }
            else{                    
                $("#politicas").show();
            }
        });
        
        $("#lnkPoliticas").click(function(e){
            e.preventDefault();
            $("#politicas").toggle("slow");
        });
        
        $("#form_contacto").submit(function(e) {
            e.preventDefault();
            $(this).prop("disabled", true);
            
            if ($("#nombre").val() === ""){
                alert('Debe ingresar el nombre', 'Aceptar');
                return;
            }
            
            if ($("#email").val() === ""){
                alert('Debe ingresar el correo electrónico', 'Aceptar');
                return;
            }
            
            if ($("#telefono").val() === ""){
                alert('Debe ingresar el teléfono', 'Aceptar');
                return;
            }
            
            $("#loader").addClass("loading");
            
            $.ajax({
                url: $(this).attr("action"),
                data: $(this).serialize(),
                method: 'post',
                success: function(res){
                    alert(res, 'Aceptar');
                    $("#nombre").val('');
                    $("#email").val('');
                    $("#telefono").val('');
                    $(this).prop("disabled", false);
                    $("#loader").removeClass("loading");                    
                }
            });
        });

        $(document).foundation();
    });
})(window, jQuery);