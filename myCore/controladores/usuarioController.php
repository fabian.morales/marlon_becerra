<?php
class usuarioController extends myController{
    function index(){
        /*paises = Pais::all();
        $jusuario = JFactory::getUser();
        $musuario = myUsuario::find($jsuario->id);        
        $estados = sizeof($musuario) ? Departamento::where("id_pais", $musuario->id_pais)->get() : [];
        return myView::render("usuario.form_usuario", ["jusuario" => $jusuario, "paises" => $paises, "estados" => $estados]);*/
        return $this->mostrarLogin();
    }
    
    function mostrarLogin($redirect = ""){
        if (empty($redirect)){
            $redirect = myApp::getRequest()->getVar("redirect", base64_encode("index.php?controller=carrito"), "base64");    
        }
        else{
            $redirect = base64_encode($redirect);
        }
        
        $url = myApp::getUrlRoot();
        
        echo myApp::estaLogueado();
        if (!myApp::estaLogueado()){
            return myView::render("usuario.form_login", ["redirect" => $redirect, "url" => $url]);
        }
        else{
            return myView::render("usuario.logout", ["user" => myApp::obtenerUsuario(), "url" => $url]);
        }
    }
    
    /*function mostrarWidget(){
        $usuario = JFactory::getUser();
        return myView::render("usuario.widget", ["user" => $usuario]);
    }*/

    function login($username="", $password="", $redirect="", $noredir = false){       
        $req = myApp::getRequest();
        if (!$username){
            $username = $req->getVar('usuario_login', '', 'username');
        }

        if (!$password){
            $password = $req->getVar('clave_login', '', "RAW");
        }

        $opciones = array();
        $opciones['remember'] = false;
        $opciones['return'] = $redirect;

        $credenciales = array();
        $credenciales['user_login'] = $username;
        $credenciales['user_password'] = $password;		
        $credenciales['remember'] = false;
        $res = myApp::login($credenciales, $opciones);
        
        if((defined('DOING_AJAX') && DOING_AJAX)){
            if(!myApp::esError($res)){
                if (!$noredir){
                    if (!$redirect) {
                        $redirect = 'index.php?controller=carrito';
                    }
                    myApp::redirect($redirect);                
                }
            }
            else{        	
                myApp::mostrarMensaje($res->get_error_message(), "error");
            }
        }
        else{
            $ret = [];
            
            if(!myApp::esError($res)){
                if (!$noredir){
                    if (!$redirect) {
                        $redirect = 'index.php?controller=carrito';
                    }               
                }
                $ret["ok"] = 1;
                $ret["usuario"] = $res;
                $ret["redirect"] = $redirect;
            }
            else{
                $ret["ok"] = 0;
                $ret["error"] = $res->get_error_message();
            }
            
            return json_encode($ret);
        }
    }

    /*function logout($noredir = false){
        $error = myApp::logout();
        $req = myApp::getRequest();
        if (!$noredir){
            if(!JError::isError($error)){
                if ($redirect = $req->getVar('redirect', '', 'base64')){
                    $redirect = base64_decode($redirect);
                    if (!JURI::isInternal($redirect)) {
                        $redirect = 'index.php';
                    }
                }
                else{
                    $redirect = 'index.php';
                }

                if ($redirect && !(strpos($redirect, 'com_my_component'))){
                    myApp::redirect($redirect);
                }
            }
            else{
                myApp::redirect('index.php?option=com_my_component&controller=usuario');
            }            
        }
    }*/

    function guardarUsuario(){
        $req = myApp::getRequest();        
        $username = $req->getVar('usuario_registro', '', 'username');
        $password = $req->getVar('clave_registro', '', "RAW");
        $email = $req->getVar('email_registro', '', "RAW");
        $nombre = $req->getVar('nombre_registro', '', "RAW");
        $apellido = $req->getVar('apellido_registro', '', "RAW");
        $telefono = $req->getVar('telefono_registro', '', "RAW");
        $ciudad = $req->getVar('ciudad_registro', '', "RAW");
        
        if(!username_exists($username)) {
            $idUsuario = wp_create_user($username, $password, $email);
            //$user = new WP_User($idUsuario);
            //add_user_meta($idUsuario, 'first_name', $nombre);
            //add_user_meta($idUsuario, 'last_name', $apellido);
            
            wp_update_user(
                [
                    'ID'=> $idUsuario,
                    'first_name' => $nombre,
                    'last_name' => $apellido
                ]
            );
            
            update_usermeta($idUsuario, 'telefono', $telefono);
            update_usermeta($idUsuario, 'ciudad', $ciudad);
            
            $credenciales = array();
            $credenciales['user_login'] = $username;
            $credenciales['user_password'] = $password;		
            $credenciales['remember'] = false;
            
            $res = myApp::login($credenciales, []);
            $ret = [];
            
            if(!myApp::esError($res)){
                if (!$redirect) {
                    $redirect = 'index.php?controller=carrito';
                }
                
                $ret["ok"] = 1;
                $ret["usuario"] = $res;
                $ret["redirect"] = $redirect;
                $mensaje = myView::render("usuario.correo_cuenta_nueva", ["nombre" => $nombre, "clave" => password, "asunto" => "Cuenta creada - Mezanine"]);                
                wp_mail($email, 'Bienvenido a Mezanine', $mensaje);
            }
            else{
                $ret["ok"] = 0;
                $ret["error"] = "El usuario ya existe";
            }
        }
        else{
            $ret["ok"] = 0;
            $ret["error"] = $res->get_error_message();
        }
        
        return json_encode($ret);
    }
	
    /*function activarUsuario(){
        $idUsuario = myApp::getRequest()->getVar("idUsuario");
        $token = myApp::getRequest()->getVar("token");

        $user = Usuario::find($idUsuario);
        if (!$user->id){
            myApp::mostrarMensaje("La cuenta no es valida", "error");
            return false;
        }		

        if ($user->activation == 0){
            myApp::mostrarMensaje("Esta cuenta ya se encuentra activada", "error");
            return false;
        }

        if ($user->activation != $token){
            myApp::mostrarMensaje("El c&oacute;digo de validaci&oacute;n no es correcto", "error");
            return false;
        }

        $user->activation = 0;
        
        if ($user->save()){
            myApp::mostrarMensaje("Su cuenta ha sido activada exitosamente", "message");
        }
        else{
            myApp::mostrarMensaje("No se pudo activar su cuenta", "error");
        }

        return myView::render("usuario.blanco");
    }*/
}
