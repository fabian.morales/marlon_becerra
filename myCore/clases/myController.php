<?php

abstract class myController{
    public function retornar($tarea = "index", $args = []){
        if (!$tarea){
            $tarea = "index";
        }

        if (method_exists($this, $tarea)){
            $func = [$this, $tarea];
            return call_user_func_array($func, $args);
        }
        else{
            return "Opción no válida";
        }
    }
    
    public function ejecutar($tarea = "index", $args = []){
        echo $this->retornar($tarea, $args);
    }
    
    public static function _($path){
        list($controlador, $tarea) = explode("@", $path);
        $c = myApp::getController($controlador);
        return function() use ($c, $tarea){
            echo $c->ejecutar($tarea, func_get_args());
        };
    }
    
    public abstract function index();
}

