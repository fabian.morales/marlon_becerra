<?php

use \Slim\Slim;

class myRoute {
    public static function boot(){
        Slim::registerAutoloader();
        $app = new Slim();
        myRequest::boot($app->request->params());
        $app->get('/', myController::_("home@index"));
        $app->post('/contacto/solicitud', myController::_("contacto@enviarCorreoContacto"));		
        $app->run();
        return $app;
    }
}
