<?php

class Usuario extends myEloquent {    
    protected $table = 'users';
    protected $fillable = array('id', 'name', 'username', 'email', 'password');
    
    public function my(){
        return $this->hasOne("MyUsuario", "id");
    }
    
    /* Metadata WordPress*/
    public function nombre(){
        return $this->hasOne("UsuarioMeta", "user_id", "ID")->where("meta_key", "first_name");
    }
    
    public function apellido(){
        return $this->hasOne("UsuarioMeta", "user_id", "ID")->where("meta_key", "last_name");
    }
}
